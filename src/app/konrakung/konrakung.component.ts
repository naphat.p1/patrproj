import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,Validators, FormControl } from '@angular/forms';
import Swal from 'sweetalert2'


@Component({
  selector: 'app-konrakung',
  templateUrl: './konrakung.component.html',
  styleUrls: ['./konrakung.component.css']
})
export class KonrakungComponent implements OnInit {
  formgroup : FormGroup
  formerror:any

  
  constructor(private builder: FormBuilder) { 
    this.formgroup = this.builder.group({
      thainame : ['', Validators.required],
  title : ['', Validators.required],
  thaisurname : ['', Validators.required],
  idcard : ['', Validators.required],
  backidcard : ['', Validators.required],
  birthdatetype : ['', Validators.required],
  year : ['', Validators.required],
  month : ['', Validators.required],
  day : ['', Validators.required],
  telno : ['', Validators.required],
  email : ['', Validators.email]
    });

    this.formerror = {
    
      thainame : {},
  title : {},
  thaisurname : {},
  idcard : {},
  backidcard : {},
  birthdatetype : {},
  year : {},
  month : {},
  day : {},
  telno : {},
  email : {}
    };

  }


  onFormValuesChanged()
  {
    for ( const field in this.formerror )
        {
            if ( !this.formerror.hasOwnProperty(field) )
            {
                continue;
            }
            // Clear previous errors
            this.formerror[field] = {};
            // Get the control
            const control = this.formgroup.get(field);
            if ( control && control.dirty && !control.valid )
            {
                this.formerror[field] = control.errors;
            }
        }
  }


  ngOnInit(): void {
    this.formgroup.valueChanges.subscribe(() => {
      
      this.onFormValuesChanged();
    });
  }

  

  submitform() {
    console.log(this.formgroup);
    if (this.formgroup.valid) {
        // save data

        Swal.fire({
          title: 'All job done.',
          
          icon: 'success',
          showCancelButton: false,
          confirmButtonText: 'OK',
          cancelButtonText: 'No, keep it'
        });
    } else {
        this.validateAllFields(this.formgroup); 
        Swal.fire({
          title: 'Have some error.',
          
          icon: 'error',
          showCancelButton: false,
          confirmButtonText: 'OK',
          cancelButtonText: 'No, keep it'
        });
    }
}

validateAllFields(formGroup: FormGroup) {         
    Object.keys(formGroup.controls).forEach(field => {  
        const control = formGroup.get(field);            
        if (control instanceof FormControl) {             
            control.markAsTouched({ onlySelf: true });
        } else if (control instanceof FormGroup) {        
            this.validateAllFields(control);  
        }
    });
}

}
