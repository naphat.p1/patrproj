import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KonrakungComponent } from './konrakung.component';

describe('KonrakungComponent', () => {
  let component: KonrakungComponent;
  let fixture: ComponentFixture<KonrakungComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KonrakungComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KonrakungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
